<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Produit
 *
 * @ORM\Table(name="produit")
 * @ORM\Entity
 */
class Produit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="typeproduit", type="string", length=255, nullable=false)
     */
    private $typeproduit;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Carnetproducteurs", inversedBy="produit")
     * @ORM\JoinTable(name="produit_carnet_producteurs",
     *   joinColumns={
     *     @ORM\JoinColumn(name="produit_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="carnet_producteurs_id", referencedColumnName="id")
     *   }
     * )
     */
    private $carnetProducteurs;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->carnetProducteurs = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeproduit(): ?string
    {
        return $this->typeproduit;
    }

    public function setTypeproduit(string $typeproduit): self
    {
        $this->typeproduit = $typeproduit;

        return $this;
    }

    /**
     * @return Collection|Carnetproducteurs[]
     */
    public function getCarnetProducteurs(): Collection
    {
        return $this->carnetProducteurs;
    }

    public function addCarnetProducteur(Carnetproducteurs $carnetProducteur): self
    {
        if (!$this->carnetProducteurs->contains($carnetProducteur)) {
            $this->carnetProducteurs[] = $carnetProducteur;
        }

        return $this;
    }

    public function removeCarnetProducteur(Carnetproducteurs $carnetProducteur): self
    {
        $this->carnetProducteurs->removeElement($carnetProducteur);

        return $this;
    }

}
