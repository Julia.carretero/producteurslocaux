<?php

namespace App\Entity;

use App\Entity\Produit;
use App\Entity\Personne;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Carnetproducteurs
 *
 * @ORM\Table(name="carnetproducteurs", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_CEF5361EA21BD112", columns={"personne_id"})})
 * @ORM\Entity
 */
class Carnetproducteurs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="societe_producteur", type="text", length=65535, nullable=false)
     */
    private $societeProducteur;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle_point_vente", type="text", length=65535, nullable=false)
     */
    private $libellePointVente;

    /**
     * @var int|null
     *
     * @ORM\Column(name="numero", type="integer", nullable=true)
     */
    private $numero;

    /**
     * @var string|null
     *
     * @ORM\Column(name="voie", type="text", length=65535, nullable=true)
     */
    private $voie;

    /**
     * @var int|null
     *
     * @ORM\Column(name="insee", type="integer", nullable=true)
     */
    private $insee;

    /**
     * @var string|null
     *
     * @ORM\Column(name="commune", type="text", length=65535, nullable=true)
     */
    private $commune;

    /**
     * @var string
     *
     * @ORM\Column(name="creneau", type="text", length=65535, nullable=false)
     */
    private $creneau;

    /**
     * @var string
     *
     * @ORM\Column(name="ouverture", type="text", length=65535, nullable=false)
     */
    private $ouverture;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float", precision=10, scale=0, nullable=false)
     */
    private $latitude;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float", precision=10, scale=0, nullable=false)
     */
    private $longitude;

    /**
     * @var int
     *
     * @ORM\Column(name="prix", type="integer", nullable=false)
     */
    private $prix;

    /**
     * @var \Personne
     *
     * @ORM\ManyToOne(targetEntity="Personne")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="personne_id", referencedColumnName="id")
     * })
     */
    private $personne;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Produit", mappedBy="carnetProducteurs")
     */
    private $produit;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->produit = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSocieteProducteur(): ?string
    {
        return $this->societeProducteur;
    }

    public function setSocieteProducteur(string $societeProducteur): self
    {
        $this->societeProducteur = $societeProducteur;

        return $this;
    }

    public function getLibellePointVente(): ?string
    {
        return $this->libellePointVente;
    }

    public function setLibellePointVente(string $libellePointVente): self
    {
        $this->libellePointVente = $libellePointVente;

        return $this;
    }

    public function getNumero(): ?int
    {
        return $this->numero;
    }

    public function setNumero(?int $numero): self
    {
        $this->numero = $numero;

        return $this;
    }

    public function getVoie(): ?string
    {
        return $this->voie;
    }

    public function setVoie(?string $voie): self
    {
        $this->voie = $voie;

        return $this;
    }

    public function getInsee(): ?int
    {
        return $this->insee;
    }

    public function setInsee(?int $insee): self
    {
        $this->insee = $insee;

        return $this;
    }

    public function getCommune(): ?string
    {
        return $this->commune;
    }

    public function setCommune(?string $commune): self
    {
        $this->commune = $commune;

        return $this;
    }

    public function getCreneau(): ?string
    {
        return $this->creneau;
    }

    public function setCreneau(string $creneau): self
    {
        $this->creneau = $creneau;

        return $this;
    }

    public function getOuverture(): ?string
    {
        return $this->ouverture;
    }

    public function setOuverture(string $ouverture): self
    {
        $this->ouverture = $ouverture;

        return $this;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getPrix(): ?int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getPersonne(): ?Personne
    {
        return $this->personne;
    }

    public function setPersonne(?Personne $personne): self
    {
        $this->personne = $personne;

        return $this;
    }

    /**
     * @return Collection|Produit[]
     */
    public function getProduit(): Collection
    {
        return $this->produit;
    }

    public function addProduit(Produit $produit): self
    {
        if (!$this->produit->contains($produit)) {
            $this->produit[] = $produit;
            $produit->addCarnetProducteur($this);
        }

        return $this;
    }

    public function removeProduit(Produit $produit): self
    {
        if ($this->produit->removeElement($produit)) {
            $produit->removeCarnetProducteur($this);
        }

        return $this;
    }

}
